var request = require('request');
var cheerio = require('cheerio');
var config = require('../config');

var WebScraper = function () {
	this.init();
};

WebScraper.prototype = {
	// init method
	init: function () {
		// init code here
	},

	// scrape url
	scrape: function (url, cb) {
		request(url, function (error, response, html) {
		  if (!error && response.statusCode == 200) {
		  	this.plainHTML = html;
		  	$ = cheerio.load(html);
		  	cb();
		  } else {
		  	throw Error("can't scrape url: " + url);
		  }
		});
	},

	// particular use case
	// extract HR openings from URL.
	/// HR_URL defined as constant above.
	extractHROpenings: function (cb) {
		var self = this;
		var openings = [];

		this.scrape(config.HR_URL, function (error) {
			if (!error) {
				$('div.oferta').each(function(i, elem) {
				  var b = {
				  	title: $(this).find("a").text(),
				  	description: $(this).find(".views-field-field-descripcion .field-content").text()
				  };

				  openings.push(b);
				});
			}

			cb(error, openings);
		});
	},

	// particular use case
	// extract Leadership Info from URL.
	/// HR_URL defined as constant above.
	extractPeopleInfo: function (cb) {
		var self = this;
		var data = [];

		this.scrape(config.LEADERSHIP_URL, function (error) {
			if (!error) {
				$('.leadership div.col').each(function(i, elem) {
				  var p = {
				  	picture: $(this).find(".views-field-picture img").attr("src"),
				  	name: $(this).find(".views-field-field-nombre p").text(),
				  	title: $(this).find(".views-field-field-puesto div").text()
				  };

				  data.push(p);
				});
			}

			cb(error, data);
		});
	},

	// particular use case
	// extract Leadership Info from URL.
	/// HR_URL defined as constant above.
	extractClientsInfo: function (cb) {
		var self = this;
		var data = [];

		this.scrape(config.ABOUT_URL, function (error) {
			if (!error) {
				$('.view-clientes div.logo').each(function(i, elem) {
				  var p = {
				  	picture: $(this).find("img").attr("src")
				  };

				  data.push(p);
				});
			}

			cb(error, data);
		});
	}
};

module.exports = new WebScraper();