// bot dependencies
var builder = require('botbuilder');
// other dependencies
var requestify = require('requestify');
var config = require('../config');
var Util = require("../utils/Functions");

// OrderController JS
var OrderController = function (bot) {
    this.LOG_TAG = "OrderController::";
    this.init();
};

OrderController.prototype = {
    // init
    init: function () {
        var self = this;

        // order processing
        GlobalBotInstance.dialog('/confirm_order', [
            // starts by asking the user for its name.
            function (session, args, next) {
                if (session.userData.rawOrder) {
                    session.userData.orderString = self.processOrder(session.userData.rawOrder);

                    if (!session.userData.rawOrder.number || !session.userData.rawOrder.gusto || !session.userData.rawOrder.tamanio) {
                        session.send("Qué cantidad, tamaño y gustos?");
                        session.userData.rawOrder = null;
                        session.endDialog();
                    } else {
                        builder.Prompts.text(session, 'Bien. Confirmás tu orden por: ' + session.userData.orderString);
                    }
                }
            },

            function (session, results) {
                if (results.response.toLowerCase().indexOf("si") >= 0) {
                    builder.Prompts.text(session, "Genial, le agregamos unas brahmas no?");
                } else {
                    session.send("Ok, volvamos a intentarlo! repetime tu orden por favor :)");
                    session.endDialog();
                }
            },

            function (session, results) {
                if (results.response.toLowerCase().indexOf("si") >= 0) {
                    if (session.userData.address) {
                        builder.Prompts.text(session, "Enviamos tu pedido a la misma dirección? " + session.userData.address);
                    } else {
                        builder.Prompts.text(session, "Excelente! A qué dirección enviamos el pedido?");
                    }
                } else {
                    session.send("Ok, volvamos a intentarlo! repetime tu orden por favor :)");
                }
            },

            function (session, results) {
                var address = "";
                if (results.response && results.response.toLowerCase().indexOf("si") >= 0) {
                    address = session.userData.address;
                } else {
                    address = results.response;
                }

                session.beginDialog("/close_order", address);
                session.endDialog();
            },
        ]);

        GlobalBotInstance.dialog('/close_order', [
            function (session, args) {
                if (args) {
                    console.log("close_order::args: " + args);
                    session.userData.address = args;

                    var orderData = {
                        raw: session.userData.rawOrder,
                        address: args,
                        order: session.userData.orderString
                    };

                    self.sendOrderConfirmation(orderData, function (error) {
                        if (!error) {
                            session.send("Gracias por tu pedido, la orden ya fue procesada correctamente!");
                            session.userData.lastOrder = orderData;
                            session.userData.rawOrder = null;
                            session.userData.orderString = null;
                        } else {
                            session.send("Upps...hubo un problema procesando la orden!");
                        }
                    });
                } else {
                    session.send("Necesito una dirección válida para enviar tu pedido!");
                }
            }
        ]);

        GlobalBotInstance.dialog('/email_request', [
            function (session) {
                builder.Prompts.text(session, 'Cuál es tu email?');
            },
            function (session, results) {
                if (results.response.indexOf("@") > 0) {
                    session.userData.email = results.response;
                } else {
                    session.send('Si no te sentís cómodo dandole tus datos a un Bot, está bien ;)');
                    session.send('Podés usar nuestro formulario de contacto ubicado en nuestra web: http://pizza-bot.azurewebsite.net/');
                }
            }
        ]);
    },

    sendOrderConfirmation: function (orderData, cb) {
        requestify.post(config.API_URL + "/sendMail", {
            orderData: orderData
        })
        .then(function(response) {
            // Get the response body (JSON parsed or jQuery object for XMLs)
            response.getBody();

            // Get the raw response body
            response.body;
            cb(false);
        });
    },

    processOrder: function (order) {
        var self = this;
        console.log(this.LOG_TAG, "processOrder");
        console.log(order);

        var orderStr = "";


        if (order.number) {
            orderStr += "\n\n";
            orderStr += "Cantidad: " + self.stringToNumber(order.number.toLowerCase());
        }

        if (order.tamanio) {
            orderStr += "\n\n";
            orderStr += "Tamaño: " + order.tamanio;
        }

        if (order.gusto) {
            orderStr += "\n\n";
            orderStr += "Gusto: " + order.gusto;
        }

        return orderStr;
    },

    stringToNumber: function (str) {
        switch (str) {
            case 'una':
                return 1;
                break;
            case 'dos':
                return 2;
                break;
            case 'tres':
                return 3;
            case 'cuatro':
                return 4;
                break;
            case 'cinco':
                return 5;
                break;
            case 'seis':
                return 6;
                break;
            default:
                return str;
                break;
        }
    }
};

module.exports = new OrderController ();
