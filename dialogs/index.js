// BotFramework dependencies
var builder = require('botbuilder');

var DialogController = function (bot) {
    this.LOG_TAG = "IntentController::";

    console.log(this.LOG_TAG + "new DialogController instance");
    this.init();
};

DialogController.prototype = {
    init: function () {
        console.log(this.LOG_TAG + "::init");
        
        // each require below boots a new Dialog Handler.
		var HelloController = require('./HelloController');
		var OrderController = require('./OrderController');
		var MenuController = require('./MenuController');
    }
}

module.exports = DialogController;