// bot dependencies
var builder = require('botbuilder');
var config = require('../config');
var requestify = require('requestify');

// HelloController
var HelloController = function (bot) {
    this.LOG_TAG = "HelloController::";
    this.init();
};

HelloController.prototype = {
    // init
    init: function () {
        var self = this;
        // Add first run dialog
        GlobalBotInstance.dialog('firstRun', [
            function (session) {
                console.log("bot:dialog::firstRun");
                // Update versio number and start Prompts
                // - The version number needs to be updated first to prevent re-triggering 
                //   the dialog. 
                session.userData.version = 1.0; 

                session.beginDialog("/welcome");
            }
        ]);

        // welcome message dialog
        GlobalBotInstance.dialog("/welcome", [
            function (session) {
                console.log("bot:dialog::welcome");
                if (session.userData.lastOrder) {
                    var orderStr = self.parseLastOrder(session.userData.lastOrder.raw);
                    session.send("Hola! Bienvenido de vuelta.");
                    builder.Prompts.text(session, orderStr);
                } else {
                    session.send("Hola! En qué te puedo ayudar?");
                    session.userData.hello = true;
                    session.endDialog();
                }
            },

            function (session, results) {
                if (results.response.indexOf("si") >= 0) {
                    self.sendOrderConfirmation(session.userData.lastOrder, function (error) {
                        if (!error) {
                            session.send("Excelente, eso fue rápido! Gracias por tu pedido, la orden ya fue procesada correctamente!");
                        } else {
                            session.send("Upps...hubo un problema procesando la orden!");
                        }
                    });
                } else {
                    session.send("Bien, siempre es bueno cambiar un poco :)");
                    session.send("Indicame cuántas pizzas querés pedir y de qué gusto");
                    session.endDialog();
                }
            }
        ]);
    },

    sendOrderConfirmation: function (orderData, cb) {
        requestify.post(config.API_URL + "/sendMail", {
            orderData: orderData
        })
        .then(function(response) {
            // Get the response body (JSON parsed or jQuery object for XMLs)
            response.getBody();

            // Get the raw response body
            response.body;
            cb(false);
        });
    },

    parseLastOrder: function (order) {
        console.log(this.LOG_TAG + " parseLastOrder");
        console.log(order);
        var lastOrderStr = "Querés repetir la última order por: " + order.number + " " + order.tamanio +  " de " + order.gusto + " ?";
        return lastOrderStr; 
    }
};

module.exports = new HelloController();
