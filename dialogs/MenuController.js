// bot dependencies
var builder = require('botbuilder');

// MenuControler
var MenuController = function (bot) {
    this.LOG_TAG = "HelloController::";
    this.init();
};

MenuController.prototype = {
    // init
    init: function () {
        // Add root menu dialog
        GlobalBotInstance.dialog('rootMenu', [
            function (session) {
                session.send("Puedo ayudarte a realizar tu pedido? Indicame cuántas pizzas querés pedir y de qué gusto");

            }
        ]);
    }
};

module.exports = new MenuController();
