var express = require('express')
var bodyParser = require('body-parser');
var path = require('path');
// BotFramework dependencies
var builder = require('botbuilder');

var IntentHandler = require("./intents");
var DialogHandler = require("./dialogs");

// Setup Express Server
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

//=========================================================
// Aux API Setup
//=========================================================
var api = require('./api');
app.use('/api', api);

//=========================================================
// Bot Setup
//=========================================================

// Create chat bot
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

/** IMPORTANT - bot variable is GLOBAL !! **/
GlobalBotInstance = new builder.UniversalBot(connector);
GlobalBotInstance.set("persistConversationData", true);
GlobalBotInstance.set("persistUserData", true);

// Express Handler Set Up
app.post('/api/messages', connector.listen());

// load interactions
var intentHandler = new IntentHandler();
var dialogHandler = new DialogHandler();

//=========================================================
// HTTP Test
//=========================================================
// view engine setup
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.get('/', function (req, res, next) {
	res.render('index', { title: 'Pizza Bot Demo' });
});

app.listen(process.env.PORT || 3000, function () {
  console.log('App listening on port ' + process.env.PORT || 3000);
});
